import request from 'supertest';
import express from 'express';
import userRouter from '../routes/userRouter.js';
import apiRouter from '../routes/apiRouter.js';
import loadBalancer from '../loadBalancer.js';
import { rateLimit } from 'express-rate-limit';

const app = express();

const limiter = rateLimit({
    windowMs: 60 * 60 * 1000,
    limit: 1000,
    message: 'the limit is reached'
});

app.use(express.json());
app.use('/', userRouter);
app.use('/', apiRouter);
app.use(limiter);
app.use('/equipes', loadBalancer);
app.use('/joueurs', loadBalancer);

describe('API Tests', () => {
    test('GET /equipes should return 200', async () => {
        const res = await request(app)
            .get('/equipes')
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });

    test('GET /equipes/:id should return 200', async () => {
        const res = await request(app)
            .get('/equipes/1')
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });

    test('POST /equipes should return 201', async () => {
        const res = await request(app)
            .post('/equipes')
            .send({
                name: 'New Team'
            })
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('message', 'Team created successfully');
    });

    test('PUT /equipes/:id should return 200', async () => {
        const res = await request(app)
            .put('/equipes/1')
            .send({
                name: 'Updated Team'
            })
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Team updated successfully');
    });

    test('DELETE /equipes/:id should return 200', async () => {
        const res = await request(app)
            .delete('/equipes/1')
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Team deleted successfully');
    });

    test('GET /joueurs should return 200', async () => {
        const res = await request(app)
            .get('/joueurs')
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });

    test('GET /joueurs/:id should return 200', async () => {
        const res = await request(app)
            .get('/joueurs/1')
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message');
    });

    test('POST /joueurs should return 201', async () => {
        const res = await request(app)
            .post('/joueurs')
            .send({
                name: 'New Player'
            })
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('message', 'Player created successfully');
    });

    test('PUT /joueurs/:id should return 200', async () => {
        const res = await request(app)
            .put('/joueurs/1')
            .send({
                name: 'Updated Player'
            })
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Player updated successfully');
    });

    test('DELETE /joueurs/:id should return 200', async () => {
        const res = await request(app)
            .delete('/joueurs/1')
            .set('Authorization', `Bearer valid_token`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('message', 'Player deleted successfully');
    });
});
