import express from 'express';
import mongoose from 'mongoose';
import userRouter from './routes/userRouter.js'
import apiRouter from './routes/apiRouter.js';
import { rateLimit } from 'express-rate-limit'
import loadBalancer from './loadBalancer.js';
import { createClient } from 'redis';
import redis from 'redis';

const app = express();

const limiter = rateLimit({
	windowMs: 60 * 60 * 1000,
	limit: 1000,
	message: 'the limit is reached'
})

const port = process.env.PORT || 7000;

const redisClient = redis.createClient({
  host: 'redis_server',
  port: 6379
});

redisClient.on('error', (err) => {
  console.error('Could not connect to Redis', err);
});

redisClient.on('connect', () => {
  console.log('Connected to Redis');
});

function cache(req, res, next) {
  const { url } = req;
  redisClient.get(url, (err, data) => {
    if (err) throw err;
    if (data !== null) {
      res.send(JSON.parse(data));
    } else {
      res.sendResponse = res.send;
      res.send = (body) => {
        redisClient.setex(url, 3600, JSON.stringify(body));
        res.sendResponse(body);
      };
      next();
    }
  });
}

app.use(express.json());
app.use('/', userRouter);
app.use('/', apiRouter);
app.use(limiter);
app.use('/equipes', loadBalancer);
app.use('/joueurs', loadBalancer);
app.use('/equipes', cache);
app.use('/joueurs', cache);

// Connexion à la base de données MongoDB
mongoose.connect('mongodb://api_gateway-mongodb/users_db', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Connexion à la base de données réussie');
    app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`);
});

  })
  .catch((error) => {
    console.error('Erreur de connexion à la base de données', error);
  });

