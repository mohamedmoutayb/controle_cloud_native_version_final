import { createProxyMiddleware } from 'http-proxy-middleware';

const targets = [
    'http://equipe_joueur_service_instance_1:8000',
    'http://equipe_joueur_service_instance_2:8000',
    'http://equipe_joueur_service_instance_3:8000'
];

let currentIndex = 0;

const loadBalancer = createProxyMiddleware({
  target: targets[0], 
  changeOrigin: true,
  router: (req) => {
    const target = targets[currentIndex];
    currentIndex = (currentIndex + 1) % targets.length;
    return target;
  }
});

export default loadBalancer;